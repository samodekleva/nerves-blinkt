defmodule Blinkt do
  @moduledoc """
  Documentation for Blinkt.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Blinkt.hello
      :world

  """
  def hello do
    :world
  end
end
