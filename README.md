# Elixir Nerves Library For Pimoroni Blinkt!

## Resources

- APA121 Addressable LED Hookup Guide: https://www.digikey.si/en/maker/projects/apa102-addressable-led-hookup-guide/093dc573bcf2487ab2146cec7fd870b3
- APA121 in C++: https://github.com/cpldcpu/light_ws2812/blob/master/light_apa102_AVR/Light_apa102/light_apa102.c
- Blinkt! in Node: https://github.com/Irrelon/node-blinkt/
